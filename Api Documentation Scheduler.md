SIMPLE BANKING SCHEDULER DOCUMENTATION

### GET USERS 
```
METHOD : GET
ENDPOINT : http://localhost:8080/api/user

RESPONSE : 
[
    {
        "id": "8a6886b080a77bb30180a781231f0000",
        "name": "Muhamad Saeful",
        "nik": 327329012,
        "dailyTransaction": 0,
        "card": {
            "id": "ff8080818087fd2a018087fd427f0000",
            "membership": "gold",
            "maximumTransferDaily": 10000000
        }
    }
]
```
### GET CARDS
```
METHOD : GET
ENDPOINT : http://localhost:8080/api/card

RESPONSE :
[
    {
        "id": "ff8080818087fd2a018087fd427f0000",
        "membership": "gold",
        "maximumTransferDaily": 10000000
    },
    {
        "id": "ff8080818087fd2a018087fd633d0001",
        "membership": "platinum",
        "maximumTransferDaily": 50000000
    },
    {
        "id": "ff8080818087fd2a018087fd716d0002",
        "membership": "black",
        "maximumTransferDaily": 100000000
    }
]
```
### ADD USER
```
METHOD : POST
ENDPOINT : http://localhost:8080/api/user

REQUEST : 
{
    "name":"Muhamad Saeful",
    "nik":"327329012",
    "card":{
        "id":"ff8080818087fd2a018087fd427f0000"
    }
}


RESPONSE :

    "id": "8a6886b080a77bb30180a781231f0000",
    "name": "Muhamad Saeful",
    "nik": 327329012,
    "dailyTransaction": 0,
    "card": {
        "id": "ff8080818087fd2a018087fd427f0000",
        "membership": null,
        "maximumTransferDaily": 0
    }
}
```
### TRANSFER
```
METHOD : POST
ENDPOINT : http://localhost:8080/api/transfer

REQUEST :
{
    "id":"8a6886b080a77bb30180a781231f0000",
    "amount":1000000
}


RESPONSE :
{
	message:”Successfully Transfer”
}
```
### ADD SCHEDULE
```
METHOD : POST
ENDPOINT : http://localhost:8080/schedule/taskdef

REQUEST :
{
    "cronExpression”:”* 33 13 * * ?",
    "action":"Reset Maximum Transfer"
}

RESPONSE : 
{
    "id": "8a6886b080a793c40180a797e3350000",
    "cronExpression": "0 33 13 * * ?",
    "action": "Reset Maximum Transfer",
    "createdAt": "2022-05-09T06:53:10.800+00:00",
    "updatedAt": null
}
```
### EDIT SCHEDULE
```
METHOD : PUT
ENDPOINT : http://localhost:8080/schedule/taskdef/edit

REQUEST :
{
    "id":"8a6886b080a77bb30180a7845a060001",
    "cronExpression":"0 58 13 * * ?",
    "action":"Reset Maximum Transfer"
}

RESPONSE :
{
	message:”Successfully Updated.”
}
```
### REMOVE SCHEDULE
```
METHOD : DELETE
ENDPOINT : http://localhost:8080/schedule/remove/8a6886b080a77bb30180a7845a060001

RESPONSE :
{
	message:“Successfully Deleted.”
}
```
### GET SCHEDULE
```
METHOD : GET
ENDPOINT : http://localhost:8080/schedule/taskdef
RESPONSE :
[
    {
        "id": "8a6886b080a793c40180a797e3350000",
        "cronExpression": "0 33 13 * * ?",
        "action": "Reset Maximum Transfer",
        "createdAt": "2022-05-09T06:53:10.800+00:00",
        "updatedAt": null
    }
]
```

