package com.scheduler.Entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@NoArgsConstructor
@Data
@Entity
@Table(name = "cards")
public class Card {

    @Id
    @Column(name = "id",nullable = false,unique = true)
    @GeneratedValue(generator = "uuid-generator")
    @GenericGenerator(name = "uuid-generator", strategy = "uuid")
    private String id;

    @Column(name = "membership",nullable = false)
    private String membership;

    @Column(name = "maximum_transfer_daily",nullable = true)
    private Integer maximumTransferDaily = 0;
}
