package com.scheduler.Entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "users")
public class UserApp {

    @Id
    @Column(name = "id",nullable = false,unique = true)
    @GeneratedValue(generator = "uuid-generator")
    @GenericGenerator(name = "uuid-generator", strategy = "uuid")
    private String id;

    private String name;
    private String nik;
    private Integer amount;

    @Column(name = "daily_transaction")
    private Integer dailyTransaction;

    @ManyToOne
    @JoinColumn(name = "card_id")
    private Card card;
}
