package com.scheduler.controller;

import com.scheduler.Entity.Card;
import com.scheduler.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class CardController {

    @Autowired
    CardService cardService;

    @CrossOrigin("http://localhost:3000")
    @PostMapping("/card")
    public ResponseEntity<Card> registerCard(@RequestBody Card card){
        cardService.register(card);
        return ResponseEntity.ok().body(card);
    }

    @CrossOrigin("http://localhost:3000")
    @GetMapping("/card")
    public List<Card> findAllCard(){
        return cardService.findAll();
    }
}
