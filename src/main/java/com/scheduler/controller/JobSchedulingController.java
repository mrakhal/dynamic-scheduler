package com.scheduler.controller;

import com.scheduler.Entity.SchedulerTask;
import com.scheduler.service.TaskDefinitionBean;
import com.scheduler.service.TaskSchedulingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("schedule")
public class JobSchedulingController {

    @Autowired
    private TaskSchedulingService taskSchedulingService;

    @Autowired
    private TaskDefinitionBean taskDefinitionBean;

    @CrossOrigin("http://localhost:3000")
    @PostMapping(path="/taskdef", consumes = "application/json", produces="application/json")
    public SchedulerTask scheduleATask(@RequestBody SchedulerTask schedulerTask) {
        taskDefinitionBean.setSchedulerTask(schedulerTask);
        taskSchedulingService.scheduleATask(schedulerTask, taskDefinitionBean);
        return schedulerTask;
    }

    @CrossOrigin("http://localhost:3000")
    @PutMapping("/taskdef/edit")
    public void editScheduleATask(@RequestBody SchedulerTask schedulerTask){
        taskSchedulingService.editScheduledTask(schedulerTask, taskDefinitionBean);
    }

    @CrossOrigin("http://localhost:3000")
    @GetMapping("/taskdef")
    public List<SchedulerTask> findAllSchedule(){
        return taskSchedulingService.findAll();
    }

    @CrossOrigin("http://localhost:3000")
    @DeleteMapping(path="/remove/{jobid}")
    public void removeJob(@PathVariable String jobid) {
        taskSchedulingService.removeScheduledTask(jobid);
    }

}
