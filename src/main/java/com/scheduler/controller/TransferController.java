package com.scheduler.controller;

import com.scheduler.dto.TransferDefinition;
import com.scheduler.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class TransferController {

    @Autowired
    TransferService transferService;

    @CrossOrigin("http://localhost:3000")
    @PostMapping("/transfer")
    public String transferController(@RequestBody TransferDefinition transferDefinition){
         transferService.transferMoney(transferDefinition.getId(),transferDefinition.getAmount());
         return "Transfer Success";
    }

    @PostMapping("/reset")
    public void resetDailyTransfer(){
        transferService.resetDailyTransfer();
    }
}
