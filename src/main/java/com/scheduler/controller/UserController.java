package com.scheduler.controller;

import com.scheduler.Entity.UserApp;
import com.scheduler.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserService userService;

    @CrossOrigin("http://localhost:3000")
    @GetMapping("/user")
    public List<UserApp> getAllUser(){
        return userService.findAll();
    }

    @CrossOrigin("http://localhost:3000")
    @PostMapping("/user")
    public UserApp registerUser(@RequestBody UserApp userApp){
        return userService.register(userApp);
    }

}
