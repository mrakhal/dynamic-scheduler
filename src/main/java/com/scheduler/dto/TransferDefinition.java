package com.scheduler.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@NoArgsConstructor
public class TransferDefinition {
    private String id;
    private Integer amount;
}
