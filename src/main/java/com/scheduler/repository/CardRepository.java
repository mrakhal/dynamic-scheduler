package com.scheduler.repository;

import com.scheduler.Entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CardRepository extends JpaRepository<Card,String> {
}
