package com.scheduler.repository;

import com.scheduler.Entity.SchedulerTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SchedulerRepository extends JpaRepository<SchedulerTask,String> {
}
