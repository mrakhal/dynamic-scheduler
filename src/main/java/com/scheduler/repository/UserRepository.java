package com.scheduler.repository;

import com.scheduler.Entity.UserApp;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserApp, String> {

    public UserApp findAllById(String id);
}
