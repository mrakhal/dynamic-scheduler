package com.scheduler.repository.crud;

public interface CreateService<T,ID> {
    public T register(T t);
}
