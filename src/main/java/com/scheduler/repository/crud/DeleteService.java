package com.scheduler.repository.crud;

public interface DeleteService<T,ID> {
    public void deleteById(ID id);
}
