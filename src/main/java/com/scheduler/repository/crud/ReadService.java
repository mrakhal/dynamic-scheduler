package com.scheduler.repository.crud;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ReadService<T,ID> {
    public List<T> findAll();
}
