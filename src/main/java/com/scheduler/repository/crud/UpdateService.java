package com.scheduler.repository.crud;

public interface UpdateService<T,ID> {

    public void update(T t);
}
