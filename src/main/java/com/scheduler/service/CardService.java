package com.scheduler.service;

import com.scheduler.Entity.Card;
import com.scheduler.repository.CardRepository;
import com.scheduler.repository.crud.CreateService;
import com.scheduler.repository.crud.ReadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService implements CreateService<Card, String>, ReadService<Card, String> {

    @Autowired
    private CardRepository cardRepository;


    @Override
    public Card register(Card card) {
        return cardRepository.save(card);
    }

    @Override
    public List<Card> findAll() {
        return cardRepository.findAll();
    }

}
