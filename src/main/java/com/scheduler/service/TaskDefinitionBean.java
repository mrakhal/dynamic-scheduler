package com.scheduler.service;

import com.scheduler.Entity.SchedulerTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskDefinitionBean implements Runnable {

    @Autowired
    private TransferService transferService;
    private SchedulerTask schedulerTask;

    @Override
    public void run() {;
        System.out.println(schedulerTask.getAction().toUpperCase()+" ACTION IS HERE!!!");
        switch (schedulerTask.getAction()){
            case "Reset Daily Transaction":
                System.out.println(schedulerTask.getAction().toUpperCase()+" executed");
                transferService.resetDailyTransfer();
                break;

            case "Add Interest":
                System.out.println(schedulerTask.getAction().toUpperCase()+" executed");
                transferService.addInterest();
                break;

            case "Administrative Cost":
                System.out.println(schedulerTask.getAction().toUpperCase()+" executed");
                transferService.administrativeCost();
                break;

            default:
                System.out.println("Task Not Found");
        }
    }

    public SchedulerTask getSchedulerTask() {
        return schedulerTask;
    }

    public void setSchedulerTask(SchedulerTask schedulerTask) {
        this.schedulerTask = schedulerTask;
    }
}
