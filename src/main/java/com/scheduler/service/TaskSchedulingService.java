package com.scheduler.service;

import com.scheduler.Entity.SchedulerTask;
import com.scheduler.repository.SchedulerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ScheduledFuture;

@Service
public class TaskSchedulingService {

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    SchedulerRepository schedulerRepository;

    @Autowired
    TaskDefinitionBean taskDefinitionBean;


    public Map<String, ScheduledFuture<?>> jobsMap = new HashMap<>();

    @PostConstruct
    public void recoverScheduler(){
        System.out.println("Recover Scheduler");
        List<SchedulerTask> schedulerTask = schedulerRepository.findAll();
        for (int i = 0; i < schedulerTask.size(); i++) {
            taskDefinitionBean.setSchedulerTask(schedulerTask.get(i));
            ScheduledFuture<?> scheduledTask = taskScheduler.schedule(taskDefinitionBean,new CronTrigger(schedulerTask.get(i).getCronExpression(), TimeZone.getTimeZone(TimeZone.getDefault().getID())));
            jobsMap.put(schedulerTask.get(i).getId(),scheduledTask);
            System.out.println(schedulerTask.get(i).getAction()+" was scheduled, and cron expression: " + schedulerTask.get(i).getCronExpression());
        }
    }

    public void scheduleATask(SchedulerTask schedulerTask, Runnable tasklet){

        ScheduledFuture<?> scheduledTask = taskScheduler.schedule(tasklet,new CronTrigger(schedulerTask.getCronExpression(), TimeZone.getTimeZone(TimeZone.getDefault().getID())));

        schedulerTask.setCreatedAt(new Date(System.currentTimeMillis()));
        schedulerRepository.save(schedulerTask);
        jobsMap.put(schedulerTask.getId(),scheduledTask);
        System.out.println(schedulerTask.getAction()+" was scheduled, and cron expression: " + schedulerTask.getCronExpression());
    }

    public void editScheduledTask(SchedulerTask schedulerTask,Runnable tasklet){
        schedulerTask.setCreatedAt(schedulerRepository.findById(schedulerTask.getId()).get().getCreatedAt());
        schedulerTask.setUpdatedAt(new Date(System.currentTimeMillis()));
        schedulerRepository.save(schedulerTask);
        removeFromJobsMap(schedulerTask.getId());
        scheduleATask(schedulerTask,tasklet);
        //System.out.println(schedulerTask.getId() + scheduledTask);
    }

    public void removeScheduledTask(String jobId){
        removeFromJobsMap(jobId);
        schedulerRepository.deleteById(jobId);
    }

    private void removeFromJobsMap(String jobId) {
        ScheduledFuture<?> scheduledTask = jobsMap.get(jobId);
        if(scheduledTask != null){
            scheduledTask.cancel(true);
            jobsMap.remove(jobId);
//            System.out.println(jobId + "is Successfully Deleted.");
        }
    }



    public List<SchedulerTask> findAll(){
        return schedulerRepository.findAll();
    }
}