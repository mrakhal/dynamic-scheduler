package com.scheduler.service;

import com.scheduler.Entity.UserApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class TransferService {

    @Autowired
    private UserService userService;

    @Autowired
    private CardService cardService;


    public void transferMoney(String id, Integer amount){
        UserApp userApp = userService.findById(id);
        if(userApp.getDailyTransaction()+amount>userApp.getCard().getMaximumTransferDaily()){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,"Transaksi melebihi batas maximum transfer harian!");
        }
        userApp.setDailyTransaction(userApp.getDailyTransaction() + amount);
        userService.edit(userApp);
    }

    public void addInterest(){
        Integer interest;
        List<UserApp> userAppList = userService.findAll();
        for (int i = 0; i < userAppList.size(); i++) {
            UserApp user = userAppList.get(i);
            String cardMembership = user.getCard().getMembership();
            interest = getInterest(user, cardMembership);
            user.setAmount(user.getAmount() + interest);
            System.out.println(user.getAmount());
            userService.edit(user);
        }
    }

    public Integer getInterest(UserApp user, String cardMembership){
        Double percentageInterest = 0.0;
        Integer interest = 0;
        switch (cardMembership){
            case "Silver" : {
                interest = 10000;
                break;
            }
            case "Gold" : {
                interest = 20000;
                break;
            }
            case "Platinum" : {
                interest = 30000;
                break;
            }
        }
        return interest;
    }

    public void administrativeCost(){
        Integer administrativeCost;
        List<UserApp> userAppList = userService.findAll();
        for (int i = 0; i < userAppList.size(); i++) {
            UserApp user = userAppList.get(i);
            String cardMembership = user.getCard().getMembership();
            administrativeCost = getAdministrativeCost(user, cardMembership);
            user.setAmount(user.getAmount() - administrativeCost);
            System.out.println(user.getAmount());
            userService.edit(user);
        }
    }

    public Integer getAdministrativeCost(UserApp user, String cardMembership){
        Integer cost = 0;
        switch (cardMembership){
            case "Silver" : {
                cost = 5000;
                break;
            }
            case "Gold" : {
                cost = 15000;
                break;
            }
            case "Platinum" : {
                cost = 25000;
                break;
            }
        }
        return cost;
    }

    public void resetDailyTransfer(){
        List<UserApp> userAppList = userService.findAll();
        for (UserApp user:userAppList
             ) {
            user.setDailyTransaction(0);
            userService.edit(user);
        }
    }
}
