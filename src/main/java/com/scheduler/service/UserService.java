package com.scheduler.service;

import com.scheduler.Entity.UserApp;
import com.scheduler.repository.UserRepository;
import com.scheduler.repository.crud.CreateService;
import com.scheduler.repository.crud.ReadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements CreateService<UserApp,String>, ReadService<UserApp, String> {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserApp register(UserApp userApp) {
        userApp.setDailyTransaction(0);
        return userRepository.save(userApp);
    }

    @Override
    public List<UserApp> findAll() {
        return userRepository.findAll();
    }

    public UserApp findById(String id){
        return userRepository.findAllById(id);
    }

    public void edit(UserApp userApp){
        userRepository.save(userApp);
    }
}
